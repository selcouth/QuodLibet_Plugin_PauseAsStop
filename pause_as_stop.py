from quodlibet import _
from quodlibet import app
from quodlibet.plugins.events import EventPlugin

class PauseAsStop(EventPlugin):
    
    PLUGIN_ID   = "PauseAsStop"
    PLUGIN_NAME = _("Pause As Stop")
    PLUGIN_DESC = _("This plugin turns the pause button into a stop button (with resume on play).  This is useful if QuodLibet does not detect a new bluetooth speaker connection.")

    __enabled   = False
    __position  = -1    # Current song position. < 0 == Use application position

    def enabled(self):
        self.__enabled = True

    def disabled(self):
        self.__enabled = False

    def plugin_on_paused(self):
        if self.__enabled:
            self.__position = app.player.get_position()
            app.player.stop()

    def plugin_on_unpaused(self):
        if self.__enabled:
            # On application startup, QuodLibet will resume playing from the same
            # position in the previous song.  In order to preserve this
            # functionality, a negative seek position indicates this plugin
            # functionality should be ignored.
            if self.__position < 0:
                self.__position = app.player.get_position()
            else:
                app.player.seek(self.__position)
